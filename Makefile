
version ?= $(subst v,,$(shell git describe --tags))
commit ?= $(shell git rev-parse --short HEAD)

what := ./...

# test gofmt results: https://github.com/golang/go/issues/24230
badformat := $(shell gofmt -l .)

.PHONY: all
all: clean tidy build test install

.PHONY: prepare
prepare:
	go get -u golang.org/x/lint/golint

.PHONY: clean
clean:
	go clean $(what)

.PHONY: tidy
tidy:
	test -z $(badformat)
	golint $(what)
	go vet $(what)

.PHONY: test
test:
	go test -cover -trimpath $(what)

.PHONY: build
build:
	go build -trimpath $(what)

.PHONY: install
install:
	CGO_ENABLED=0 go install \
		-trimpath \
		-ldflags "-X main.Version=$(version) -X main.Commit=$(commit)" \
		$(what)
