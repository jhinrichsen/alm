module gitlab.com/jhinrichsen/alm/v2

go 1.13

require (
	github.com/imdario/mergo v0.3.7
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v2 v2.2.2
)
